// OrientationAidlInterface.aidl
package com.anil.aidl;

// Declare any non-default types here with import statements

interface OrientationAidlInterface {
         String orientation();
}