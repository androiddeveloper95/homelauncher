package com.anil.aidl

import android.app.Service
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.IBinder
import androidx.lifecycle.MutableLiveData

class OrientationServices : Service(), SensorEventListener {

    companion object {
        val sensorData = MutableLiveData<FloatArray>()
    }
    private val SENSOR_DELAY_MICROS = 8 * 1000 // 8ms
    private var sensorManager: SensorManager? = null
    private var rotationSensor: Sensor? = null

    private fun createSensorManager() {
        if (sensorManager == null) {
            sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
            rotationSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
            rotationSensor?.let {
                addRotationSensorListener()
            }
        }
    }

    private fun addRotationSensorListener() {
        sensorManager?.registerListener(
            this,
            rotationSensor,
            SENSOR_DELAY_MICROS
        )
    }

    private val myBinder = object : OrientationAidlInterface.Stub() {
        override fun orientation(): String {
            synchronized(this) {
                createSensorManager()
            }
            return sensorData.value?.contentToString() ?: ""
        }
    }

    override fun onBind(p0: Intent?): IBinder? {
        return myBinder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        synchronized(this) {
            stopSensorManager()
        }
        return super.onUnbind(intent)
    }

    private fun stopSensorManager() {
        if (sensorManager == null) {
            sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
            rotationSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
            rotationSensor?.let {
                stopRotationSensorListener()
            }
        }
    }

    private fun stopRotationSensorListener() {
        sensorManager?.unregisterListener(
            this,
            rotationSensor
        )
    }

    override fun onSensorChanged(sensorEvent: SensorEvent?) {
        sensorEvent?.let {
            if (it.sensor.type == Sensor.TYPE_ROTATION_VECTOR) {
                sensorData.value = it.values
            }
        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }
}