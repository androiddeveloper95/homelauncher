package com.anil.sdk

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import com.anil.sdk.model.ApplicationInfo
import java.lang.ref.WeakReference

class InstalledApk(val ctx: Context) : BroadcastReceiver() {

    private var listener: PackageChangeListener? = null
    private var TAG = "InstalledPackageManager"

    fun getAllInstalledApkInfo(): MutableList<ApplicationInfo> {
        val pkgAppsList = mutableListOf<ApplicationInfo>()

        val intent = Intent(Intent.ACTION_MAIN, null)
        intent.addCategory(Intent.CATEGORY_LAUNCHER)

        ctx.packageManager?.let { pm ->
            pm.queryIntentActivities(intent, 0).forEach {
                val appName = it.loadLabel(pm).toString()
                val packageName = it.activityInfo.packageName
                val icon = it.activityInfo.loadIcon(pm)
                val appActivityName = it.activityInfo.name
                val packageInfo = pm.getPackageInfo(it.activityInfo.packageName, 0)
                val version = packageInfo.versionName
                val versionCode = packageInfo.versionCode.toString()

                val app = ApplicationInfo(
                    appName,
                    packageName,
                    icon,
                    appActivityName,
                    version,
                    versionCode
                )
                pkgAppsList.add(app)
            }
        }
        return pkgAppsList.sortedBy { it.appName }.toMutableList()
    }

    fun registerPackageChangeBroadcast(listener: PackageChangeListener) {
        Log.d(TAG, "register()")
        this.listener = listener

        val intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED)
        intentFilter.addAction(Intent.ACTION_PACKAGE_INSTALL)
        intentFilter.addAction(Intent.ACTION_PACKAGE_CHANGED)
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED)
        intentFilter.addDataScheme("package")

        ctx.registerReceiver(this, intentFilter)
    }

    fun unRegisterPackageChangeBroadcast() {
        Log.d(TAG, "unRegister()")
        ctx.unregisterReceiver(this)
        this.listener = null
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val packageName = intent?.data?.encodedSchemeSpecificPart
        listener?.packageInstalled(getAllInstalledApkInfo())
        Log.d(TAG, "onReceive() $packageName")
    }
}