package com.anil.sdk

import com.anil.sdk.model.ApplicationInfo

interface PackageChangeListener {
    fun packageInstalled(list: MutableList<ApplicationInfo>)
}