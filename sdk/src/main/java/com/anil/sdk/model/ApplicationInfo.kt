package com.anil.sdk.model

import android.graphics.drawable.Drawable

data class ApplicationInfo(
    var appName: String,
    var packageName: String,
    var icon: Drawable,
    val mainActivityName: String,
    var version: String,
    var versionCode: String
)
