package com.anil.homelauncher

import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.anil.aidl.OrientationAidlInterface
import com.anil.aidl.OrientationServices
import com.anil.homelauncher.databinding.ActivitySensorBinding

class SensorActivity : AppCompatActivity() {
    private var orientationInterface: OrientationAidlInterface? = null
    lateinit var binding:ActivitySensorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySensorBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            orientationInterface = OrientationAidlInterface.Stub.asInterface(service)
            getDataFromAIDL()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            orientationInterface = null
        }
    }
    override fun onResume() {
        super.onResume()
        val intent = Intent(this, OrientationServices::class.java)
        bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE)
        addSensorDataObserver()
    }
    private fun getDataFromAIDL() {
        orientationInterface?.orientation()?.let {
            binding.orientationText.text = it
        }
    }
    private fun addSensorDataObserver() {
        OrientationServices.sensorData.observe(this, Observer {
            binding.orientationText.text = it.contentToString()
        })
    }
    override fun onPause() {
        super.onPause()
        unbindService(serviceConnection)
    }
}