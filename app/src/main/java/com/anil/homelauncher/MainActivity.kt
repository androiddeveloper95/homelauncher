package com.anil.homelauncher

import android.content.Intent
import android.os.Bundle
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.anil.homelauncher.databinding.ActivityMainBinding
import com.anil.sdk.InstalledApk
import com.anil.sdk.PackageChangeListener
import com.anil.sdk.model.ApplicationInfo

class MainActivity : AppCompatActivity(), PackageChangeListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: AppsAdapter
    private lateinit var installedAp: InstalledApk

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        installedAp = InstalledApk(application)

        binding.apply {
            recycleView.layoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
            var appsList = installedAp.getAllInstalledApkInfo()
            adapter = AppsAdapter(appsList)
            recycleView.adapter = adapter
        }

        binding.serchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(txt: String?): Boolean {
                adapter.getFilter()?.let {
                    it.filter(txt)
                }
                return true
            }

        })

        installedAp.registerPackageChangeBroadcast(this)
        binding.btnSensorData.setOnClickListener {
            var intent = Intent(this, SensorActivity::class.java)
            startActivity(intent)
        }

    }

    override fun packageInstalled(list: MutableList<ApplicationInfo>) {
        binding.recycleView.adapter = AppsAdapter(list)
    }

    override fun onDestroy() {
        super.onDestroy()
        installedAp.unRegisterPackageChangeBroadcast()
    }
}
