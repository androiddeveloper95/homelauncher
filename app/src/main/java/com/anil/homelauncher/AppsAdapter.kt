package com.anil.homelauncher

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import androidx.recyclerview.widget.RecyclerView
import com.anil.homelauncher.databinding.ListItemBinding
import com.anil.sdk.model.ApplicationInfo
import java.util.*

class AppsAdapter(private var appsList: MutableList<ApplicationInfo>) :
    RecyclerView.Adapter<AppViewHolder>() {
    private var listFilter: MutableList<ApplicationInfo> = appsList
    override fun onBindViewHolder(holder: AppViewHolder, i: Int) {
        holder.tvAppName.text = appsList[i].appName
        holder.img.setImageDrawable(appsList[i].icon)
        holder.tvPackageName.text = appsList[i].packageName
        holder.tvAppActivityName.text = appsList[i].mainActivityName
        holder.tvVersionName.text = appsList[i].version
        holder.tvVersionCode.text = appsList[i].versionCode

        holder.card.setOnClickListener {
            val intent =
                it.context.packageManager.getLaunchIntentForPackage(appsList[i].packageName)
            it.context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return appsList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        var binding = ListItemBinding.inflate(inflater, parent, false)
        return AppViewHolder(binding)
    }

    fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val filterResults = FilterResults()
                if (constraint == null || constraint.isEmpty()) {
                    filterResults.count = listFilter.size
                    filterResults.values = listFilter
                } else {
                    val resultsModel = mutableListOf<ApplicationInfo>()
                    val searchStr = constraint.toString().lowercase(Locale.getDefault())
                    for (itemsModel in listFilter) {
                        if (itemsModel.appName.toLowerCase().contains(searchStr)) {
                            resultsModel.add(itemsModel)
                        }
                        filterResults.count = resultsModel.size
                        filterResults.values = resultsModel
                    }
                }
                return filterResults
            }

            override fun publishResults(
                constraint: CharSequence,
                results: FilterResults
            ) {
                appsList = results.values as MutableList<ApplicationInfo>
                notifyDataSetChanged()
            }
        }
    }

}

class AppViewHolder(itemView: ListItemBinding) : RecyclerView.ViewHolder(itemView.root) {
    var img = itemView.img
    var tvAppName = itemView.tvAppName
    var tvPackageName = itemView.tvPackageName
    var tvAppActivityName = itemView.tvAppActivityName
    var tvVersionName = itemView.tvVersionName
    var tvVersionCode = itemView.tvVersionCode
    var card = itemView.card
}